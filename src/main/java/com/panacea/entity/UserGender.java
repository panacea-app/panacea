package com.panacea.entity;

/**
 * @author Rashidi Zin
 */
public enum UserGender {

    MALE,
    FEMALE
    ;
}

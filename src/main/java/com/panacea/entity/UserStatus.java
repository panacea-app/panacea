package com.panacea.entity;

/**
 * @author Rashidi Zin
 */
public enum UserStatus {

    PENDING,
    ACTIVE,
    DORMANT,
    DELETED
    ;
}

// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.panacea.entity;

import com.panacea.entity.Record;
import com.panacea.entity.RecordDetail;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect RecordDetail_Roo_Finder {
    
    public static Long RecordDetail.countFindRecordDetailsByRecord(Record record) {
        if (record == null) throw new IllegalArgumentException("The record argument is required");
        EntityManager em = RecordDetail.entityManager();
        TypedQuery q = em.createQuery("SELECT COUNT(o) FROM RecordDetail AS o WHERE o.record = :record", Long.class);
        q.setParameter("record", record);
        return ((Long) q.getSingleResult());
    }
    
    public static TypedQuery<RecordDetail> RecordDetail.findRecordDetailsByRecord(Record record) {
        if (record == null) throw new IllegalArgumentException("The record argument is required");
        EntityManager em = RecordDetail.entityManager();
        TypedQuery<RecordDetail> q = em.createQuery("SELECT o FROM RecordDetail AS o WHERE o.record = :record", RecordDetail.class);
        q.setParameter("record", record);
        return q;
    }
    
    public static TypedQuery<RecordDetail> RecordDetail.findRecordDetailsByRecord(Record record, String sortFieldName, String sortOrder) {
        if (record == null) throw new IllegalArgumentException("The record argument is required");
        EntityManager em = RecordDetail.entityManager();
        String jpaQuery = "SELECT o FROM RecordDetail AS o WHERE o.record = :record";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        TypedQuery<RecordDetail> q = em.createQuery(jpaQuery, RecordDetail.class);
        q.setParameter("record", record);
        return q;
    }
    
}

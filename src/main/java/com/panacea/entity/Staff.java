package com.panacea.entity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author Rashidi Zin
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findStaffsByIcNoEquals" })
public class Staff extends User {

    @ManyToMany
    @NotNull(message = "Clinic information is required")
    private Set<Clinic> clinics;

    @Override
    public UserRole getRole() {
        return UserRole.STAFF;
    }
}

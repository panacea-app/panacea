package com.panacea.entity;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Rashidi Zin
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findUsersByUsernameEqualsAndPasswordEqualsAndStatus" })
public class User {

    private Long id;

    @NotEmpty(message = "Username is required")
    private String username;

    @NotEmpty(message = "Password is required")
    private String password;

    @NotEmpty(message = "Name is required")
    private String name;

    @NotEmpty(message = "Phone number is required")
    private String phoneNumber;

    @NotEmpty(message = "Address is required")
    private String address;

    @NotEmpty(message = "Identification number is required")
    private String icNo;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Gender is required")
    private UserGender gender;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Status is required")
    private UserStatus status;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Role is required")
    private UserRole role;

    @NotNull(message = "Birth date is required")
    @DateTimeFormat(style = "M-")
    private Date birthDate;

    @DateTimeFormat(style = "M-")
    private Date createdDate;

    @DateTimeFormat(style = "M-")
    private Date updatedDate;

    public UserRole getRole() {
        return role;
    }

    public UserGender getGender() {
        return gender;
    }

    public void setGender(UserGender gender) {
        this.gender = gender;
    }
}

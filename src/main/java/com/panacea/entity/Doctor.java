package com.panacea.entity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * @author Rashidi Zin
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDoctorsByIcNoEquals" })
public class Doctor extends Staff {

    @Override
    public UserRole getRole() {
        return UserRole.DOCTOR;
    }
}

package com.panacea.entity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * @author Rashidi Zin
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findRecordsByPatient" })
public class Record {

    private Long id;

    @ManyToOne
    @NotNull(message = "Patient information is required")
    private Patient patient;

    @ManyToOne
    @NotNull(message = "Doctor information is required")
    private Doctor doctor;
}

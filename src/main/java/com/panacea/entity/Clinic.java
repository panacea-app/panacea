package com.panacea.entity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Clinic extends User {

    @Override
    public UserRole getRole() {
        return UserRole.CLINIC_ADMIN;
    }

}

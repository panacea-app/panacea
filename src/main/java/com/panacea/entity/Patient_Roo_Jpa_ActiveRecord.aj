// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.panacea.entity;

import com.panacea.entity.Patient;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Patient_Roo_Jpa_ActiveRecord {
    
    public static final List<String> Patient.fieldNames4OrderClauseFilter = java.util.Arrays.asList("");
    
    public static long Patient.countPatients() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Patient o", Long.class).getSingleResult();
    }
    
    public static List<Patient> Patient.findAllPatients() {
        return entityManager().createQuery("SELECT o FROM Patient o", Patient.class).getResultList();
    }
    
    public static List<Patient> Patient.findAllPatients(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Patient o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Patient.class).getResultList();
    }
    
    public static Patient Patient.findPatient(Long id_) {
        if (id_ == null) return null;
        return entityManager().find(Patient.class, id_);
    }
    
    public static List<Patient> Patient.findPatientEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Patient o", Patient.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<Patient> Patient.findPatientEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Patient o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Patient.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public Patient Patient.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Patient merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}

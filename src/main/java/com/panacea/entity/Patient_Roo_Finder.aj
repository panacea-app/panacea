// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.panacea.entity;

import com.panacea.entity.Patient;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect Patient_Roo_Finder {
    
    public static Long Patient.countFindPatientsByIcNoEquals(String icNo) {
        if (icNo == null || icNo.length() == 0) throw new IllegalArgumentException("The icNo argument is required");
        EntityManager em = Patient.entityManager();
        TypedQuery q = em.createQuery("SELECT COUNT(o) FROM Patient AS o WHERE o.icNo = :icNo", Long.class);
        q.setParameter("icNo", icNo);
        return ((Long) q.getSingleResult());
    }
    
    public static TypedQuery<Patient> Patient.findPatientsByIcNoEquals(String icNo) {
        if (icNo == null || icNo.length() == 0) throw new IllegalArgumentException("The icNo argument is required");
        EntityManager em = Patient.entityManager();
        TypedQuery<Patient> q = em.createQuery("SELECT o FROM Patient AS o WHERE o.icNo = :icNo", Patient.class);
        q.setParameter("icNo", icNo);
        return q;
    }
    
    public static TypedQuery<Patient> Patient.findPatientsByIcNoEquals(String icNo, String sortFieldName, String sortOrder) {
        if (icNo == null || icNo.length() == 0) throw new IllegalArgumentException("The icNo argument is required");
        EntityManager em = Patient.entityManager();
        String jpaQuery = "SELECT o FROM Patient AS o WHERE o.icNo = :icNo";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        TypedQuery<Patient> q = em.createQuery(jpaQuery, Patient.class);
        q.setParameter("icNo", icNo);
        return q;
    }
    
}

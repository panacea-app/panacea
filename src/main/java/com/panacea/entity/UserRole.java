package com.panacea.entity;

/**
 * @author Rashidi Zin
 */
public enum UserRole {

    ADMIN,
    CLINIC_ADMIN,
    STAFF,
    DOCTOR,
    PATIENT
    ;
}

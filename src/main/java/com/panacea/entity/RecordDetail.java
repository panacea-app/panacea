package com.panacea.entity;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * @author Rashidi Zin
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findRecordDetailsByRecord" })
public class RecordDetail {

    @ManyToOne
    private Record record;

    @NotEmpty(message = "Diagnostic is required")
    private String diagnostic;

    private String medication;

    private int mc;

    private String remarks;

    @DateTimeFormat(style = "M-")
    private Date createdDate;

    @DateTimeFormat(style = "M-")
    private Date updatedDate;
}

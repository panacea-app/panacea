package com.panacea.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.panacea.entity.Patient;
import com.panacea.entity.User;

@RequestMapping("/staffview/**")
@Controller
public class StaffViewController {

	 @RequestMapping(value="regform", method = RequestMethod.GET )
	  public String loadPatientRegForm(Model uiModel) {
	  	
	  	uiModel.addAttribute("page", "page1");
	      return "staffview/regform";
	  }
	 
	 @RequestMapping(value="postregform",method = RequestMethod.POST)
	  public String create(@Valid Patient patient, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
	  	
		  
		  //store patient
		  //patient.persist();
	  	  uiModel.addAttribute("page", "page1");
	      return "staffview/regsuccess";
	  }
	  
	 
	 @RequestMapping(value="searchform", method = RequestMethod.GET )
	  public String loadSearchPatientForm(Model uiModel) {
	  	
	  	System.out.println("Hello staff...");
	  	
	  	uiModel.addAttribute("page", "page1");
	      return "staffview/searchform";
	  }
	 
	 
	 @RequestMapping(value="postsearchform",method = RequestMethod.POST)
	 public String searchPatient(@Valid Patient patient, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
		  	
		  //get patient by ic
		 //display patient
		 
		  
		  //patient.persist();
	  	  uiModel.addAttribute("page", "page1");
	      return "staffview/regsuccess";
	  }
	 
	 @RequestMapping(value="postaddpatient",method = RequestMethod.POST)
	 public String addPatientToClinic(@Valid Patient patient, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
		  	
		//get patient by ic
		 //get staff id and his clinic
		 //add patient and to its clinic
		  
		  //patient.persist();
	  	  uiModel.addAttribute("page", "page1");
	      return "staffview/regsuccess";
	  }
}

package com.panacea.controller;
import com.panacea.entity.RecordDetail;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;

@RequestMapping("/recorddetails")
@Controller
@RooWebScaffold(path = "recorddetails", formBackingObject = RecordDetail.class)
@RooWebFinder
public class RecordDetailController {
}

// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.panacea.controller;

import com.panacea.controller.PatientController;
import com.panacea.entity.Patient;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

privileged aspect PatientController_Roo_Controller_Finder {
    
    @RequestMapping(params = { "find=ByIcNoEquals", "form" }, method = RequestMethod.GET)
    public String PatientController.findPatientsByIcNoEqualsForm(Model uiModel) {
        return "patients/findPatientsByIcNoEquals";
    }
    
    @RequestMapping(params = "find=ByIcNoEquals", method = RequestMethod.GET)
    public String PatientController.findPatientsByIcNoEquals(@RequestParam("icNo") String icNo, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("patients", Patient.findPatientsByIcNoEquals(icNo, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Patient.countFindPatientsByIcNoEquals(icNo) / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("patients", Patient.findPatientsByIcNoEquals(icNo, sortFieldName, sortOrder).getResultList());
        }
        addDateTimeFormatPatterns(uiModel);
        return "patients/list";
    }
    
}

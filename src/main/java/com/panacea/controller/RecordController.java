package com.panacea.controller;
import com.panacea.entity.Record;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;

@RequestMapping("/records")
@Controller
@RooWebScaffold(path = "records", formBackingObject = Record.class)
@RooWebFinder
public class RecordController {
}

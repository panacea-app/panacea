package com.panacea.controller;

import com.panacea.entity.User;
import com.panacea.entity.UserRole;
import com.panacea.entity.UserStatus;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.TypedQuery;
import java.util.List;

@RequestMapping("/login")
@Controller
public class LoginController {

    @RequestMapping
    public String index() {
        return "login/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/logout")
    public String logout() {
        SecurityContextHolder.clearContext();
        return "redirect:/login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String post(@RequestParam("username") String username, @RequestParam("password") String password, Model model) {

        if (StringUtils.equalsIgnoreCase("admin", username) && StringUtils.equalsIgnoreCase("admin", password)) {
            User user = new User();
            user.setUsername("admin");
            user.setName("Admin");

            SecurityContextHolder.getContext().setAuthentication(
                    new UsernamePasswordAuthenticationToken(user, "N/A",
                            AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN")
                    )
            );

            model.addAttribute("user", user);
            return "index";
        }

        TypedQuery<User> userQuery = User.findUsersByUsernameEqualsAndPasswordEqualsAndStatus(username, password, UserStatus.ACTIVE, null, null);
        List<User> users = userQuery.getResultList();

        if (CollectionUtils.isNotEmpty(users)) {
            SecurityContextHolder.getContext().setAuthentication(
                    new UsernamePasswordAuthenticationToken(users.get(0), "N/A",
                            AuthorityUtils.commaSeparatedStringToAuthorityList(users.get(0).getRole().toString())
                    )
            );

            model.addAttribute("user", users.get(0));
            return "login/success";
        } else {
            model.addAttribute("errorMessage", "Invalid username / password");
            return "login/index";
        }
    }
}

package com.panacea.controller;
import com.panacea.entity.Clinic;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/clinics")
@Controller
@RooWebScaffold(path = "clinics", formBackingObject = Clinic.class)
public class ClinicController {
}

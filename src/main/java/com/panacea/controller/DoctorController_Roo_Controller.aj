// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.panacea.controller;

import com.panacea.controller.DoctorController;
import com.panacea.entity.Clinic;
import com.panacea.entity.Doctor;
import com.panacea.entity.UserGender;
import com.panacea.entity.UserRole;
import com.panacea.entity.UserStatus;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect DoctorController_Roo_Controller {
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String DoctorController.create(@Valid Doctor doctor, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, doctor);
            return "doctors/create";
        }
        uiModel.asMap().clear();
        doctor.persist();
        return "redirect:/doctors/" + encodeUrlPathSegment(doctor.getId_().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String DoctorController.createForm(Model uiModel) {
        populateEditForm(uiModel, new Doctor());
        return "doctors/create";
    }
    
    @RequestMapping(value = "/{id_}", produces = "text/html")
    public String DoctorController.show(@PathVariable("id_") Long id_, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("doctor", Doctor.findDoctor(id_));
        uiModel.addAttribute("itemId", id_);
        return "doctors/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String DoctorController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("doctors", Doctor.findDoctorEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) Doctor.countDoctors() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("doctors", Doctor.findAllDoctors(sortFieldName, sortOrder));
        }
        addDateTimeFormatPatterns(uiModel);
        return "doctors/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String DoctorController.update(@Valid Doctor doctor, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, doctor);
            return "doctors/update";
        }
        uiModel.asMap().clear();
        doctor.merge();
        return "redirect:/doctors/" + encodeUrlPathSegment(doctor.getId_().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id_}", params = "form", produces = "text/html")
    public String DoctorController.updateForm(@PathVariable("id_") Long id_, Model uiModel) {
        populateEditForm(uiModel, Doctor.findDoctor(id_));
        return "doctors/update";
    }
    
    @RequestMapping(value = "/{id_}", method = RequestMethod.DELETE, produces = "text/html")
    public String DoctorController.delete(@PathVariable("id_") Long id_, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Doctor doctor = Doctor.findDoctor(id_);
        doctor.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/doctors";
    }
    
    void DoctorController.addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("doctor_birthdate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute("doctor_createddate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute("doctor_updateddate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }
    
    void DoctorController.populateEditForm(Model uiModel, Doctor doctor) {
        uiModel.addAttribute("doctor", doctor);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("clinics", Clinic.findAllClinics());
        uiModel.addAttribute("usergenders", Arrays.asList(UserGender.values()));
        uiModel.addAttribute("userroles", Arrays.asList(UserRole.values()));
        uiModel.addAttribute("userstatuses", Arrays.asList(UserStatus.values()));
    }
    
    String DoctorController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}

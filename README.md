Panacea
=======
A centralised medical records system.

Adding / updating an entity
---------------------------
Once a new entity has been created run the following command

```sh
roo
roo> web mvc all --package ~.controller
```

Building the project
--------------------
```sh
mvn clean install
```

Deploying Panacea
-----------------
```sh
mvn org.apache.tomcat.maven:tomcat7-maven-plugin:2.2:run
```